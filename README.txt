
This module provides integration with the Asterisk software PBX,
including both some basic built-in call/record/playback functionality,
and an API allowing other modules to make use of these functions.

Some things you can do with this module currently:

1. Call yourself: Visit your account page. Under the 'Telephone' section,
   click 'call me', and Asterisk will call you and play a demo.
2. Call another user: Visit a user's account page. Under the 'Telephone'
   section, click 'call user', and Asterisk will first call your number,
   then call the user's number and bridge the call.
3. Call another person: Enter the person's phone number in the 'Make call'
   block. Click 'call now'. Asterisk will first call your number, then
   call the number you entered and bridge the call.
4. Record a message and attach it to a node: Create, for example, a blog
   entry. Save it, and on the blog page, click on 'record attachment'.
   Enter a filename for the attachment, select whether you want it
   initially displayed in the listings or not, and click 'Record file'.
   Asterisk will call you, and when you hear the beep, record your
   message, then hang up. Wait a few moments and refresh the page. The
   file you recorded will now be attached to the blog entry.
5. Listen to a recorded asterisk file by phone: Click on one of the links
   under the 'Recent audio files' block. Asterisk will call you and play
   the recording.

See INSTALL.txt for installation instructions.

It is a distributed system. The Drupal site communicates with the
Asterisk server via XML-PRC-- therefore, the Drupal site can be on a
regular webserver while the remote Asterisk server (properly configured
for Drupal integration) completes the calls.

Please note: setting up an Asterisk server can be a challenging and
frustrating process if you've never done it before--it's highly
recommended that you find somebody who knows what they are doing to help
you! Also, there is no support of any kind provided for the server-side
functionality of this module--it is expected that if you're attempting to
set it up, then you have the requisite Asterisk/Drupal/sysadmin skillsets
to troubleshoot any issues on your own!

If you don't have an Asterisk server properly set up for Drupal
integration you can email me for a demonstration, or you can visit
the demo site and set up an account there for demonstration purposes.

This module was originally developed by Herman Webley, and has been
rewritten for Drupal 4.7 and beyond (and is currently maintained by)
Chad Phillips.

Chad Phillips <thehunmonkgroup at gmail dot com>
hunmonk in #drupal on irc.freenode.net
Demo site: http://asterisk.drupal.xcarnated.com
