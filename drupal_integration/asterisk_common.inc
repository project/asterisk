<?php

/**
 * @file
 *   Provides functions for other files.
 *
 *   Just the asterisk_xmlrpc.php script uses it now, but in the future the
 *   PHP AGI scripts will use it as well.
 */

function asterisk_process_playback_message($user, $file) {

  $return = FALSE;

  $tempdir = DRUPAL_ACCOUNTS_DIR . DIRECTORY_SEPARATOR . $user . DIRECTORY_SEPARATOR .'cache';
  $filename = $tempdir . DIRECTORY_SEPARATOR . $file['server_file_name'];

  if ($tempname = tempnam($tempdir, $file['server_file_name'])) {
    if (asterisk_write_file($file['bits'], $tempname)) {
      if (convert_sound_file($tempname, $file['filemime'], $filename, $user) == 0) {
        $return = TRUE;
      }
      else {
        $return['error'] = "Sox unable to convert file $tempname to $filename";
      }
    }
    else {
      $return['error'] = "Can't write temporary playback file $tempname.";
    }
    unlink($tempname);
  }
  else {
    $return['error'] = "Can't create temporary file";
  }

  return $return;
}

function asterisk_retrieve_user_message($user, $message_key) {

  $return = array('status' => 'failure');

  $message = DRUPAL_ACCOUNTS_DIR . DIRECTORY_SEPARATOR . $user . DIRECTORY_SEPARATOR .'messages'. DIRECTORY_SEPARATOR . $message_key;
  if (file_exists($message)) {
    $message_data = parse_ini_file($message);
    switch ($message_data['type']) {
      case 'recording':
        if ($file = generate_recorded_file($user, $message_data)) {
          // Move the uploaded file to the cache--saves it from being downloaded from the Drupal site initially.
          $cache_file = DRUPAL_ACCOUNTS_DIR . DIRECTORY_SEPARATOR . $user . DIRECTORY_SEPARATOR .'cache'. DIRECTORY_SEPARATOR . basename($file['asterisk_filepath']);
          rename($file['asterisk_filepath'], $cache_file);
          $return = $file;
        }
        break;
    }
  }

  return $return;
}

/**
 * Validates a new call from a Drupal site.
 *
 * @param $user The username for the Drupal site.
 * @param $pass The password for the Drupal site.
 *
 * @return True if the validation was successful.
 */
function asterisk_validate_host($user, $pass) {

  $return = FALSE;
  trim($user);
  trim($pass);

  // Scan the clients directory for the user.
  if (is_dir(DRUPAL_ACCOUNTS_DIR)) {
    if ($dh = opendir(DRUPAL_ACCOUNTS_DIR)) {
      // Assume invalid username until we're proven wrong.
      $return['error'] = 'Invalid username.';
      while (($dir = readdir($dh)) !== false) {
        if (is_dir(DRUPAL_ACCOUNTS_DIR . DIRECTORY_SEPARATOR . $dir) && $user == trim($dir)) {
          $ini_file = DRUPAL_ACCOUNTS_DIR . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR .'account.conf';
          // Config file exists.
          if (file_exists($ini_file)) {
            $ini_array = parse_ini_file($ini_file, TRUE);
            // Validate the user's password.
            if (trim($ini_array['general']['pass']) == $pass) {
              // Don't check any more accounts, and clear any previously marked errors.
              $return['success'] = TRUE;
              unset($return['error']);
              break;
            }
            else {
              $return['error'] = 'Invalid password';
            }
          }
          else {
            $return['error'] = 'No configuration file exists for user.';
          }
          break;
        }
      }
      closedir($dh);
    }
    else {
      $return['error'] = "Can't read server account directory.";
    }
  }
  else {
    $return['error'] = 'Invalid server account directory.';
  }

  return $return;
}

/**
 * Validates the existence of the user's directories.
 *
 * @param $user The username for the Drupal site.
 *
 * @return True if the validation/directory creation was successful.
 */
function asterisk_validate_user_directories($user) {

  $return = TRUE;
  $user_directory = DRUPAL_ACCOUNTS_DIR . DIRECTORY_SEPARATOR . $user;
  $directories = array('cache', 'messages', 'recordings');

  if (is_dir($user_directory)) {
    foreach ($directories as $directory) {
      $this_directory = $user_directory . DIRECTORY_SEPARATOR . $directory;
      if (!is_dir($this_directory)) {
        unset($return);
        $return['error'] = "User support directory $this_directory does not exist.";
        break;
      }
    }
  }
  else {
    unset($return);
    $return['error'] = "Can't find previously created user directory $user_directory.";
  }

  return $return;
}

/**
 * Writes a call file to the Asterisk outgoing spool.
 *
 * @param $user The username for the Drupal site.
 * @param $callfile A string representing the call file.
 * @return True if the call file was successfully copied to the spool.
 */
function asterisk_write_call_file($user, $callfile) {

  $return = FALSE;

  $tempdir = DRUPAL_ACCOUNTS_DIR . DIRECTORY_SEPARATOR . $user . DIRECTORY_SEPARATOR .'cache';

  // Write the information to a temporary file.
  $tmpfname = tempnam($tempdir, 'drupal-record');

  $result = asterisk_write_file($callfile, $tmpfname);
  if ($result === TRUE) {
    chmod($tmpfname, 0777);
    $outgoing = CALL_SPOOL_DIR . DIRECTORY_SEPARATOR . basename($tmpfname);
    // Move (NOT copy or create in) to outgoing spool.
    if (rename($tmpfname, $outgoing)) {
      $return = TRUE;
    }
    else {
      $return['error'] = "Can't move call file to spool: $outgoing.";
      unlink($tmpfname);
    }
  }
  else {
    $return['error'] = "Can't write call file to user cache $tempdir.";
  }

  return $return;
}

/**
 * Writes a file to the specified location.
 * @param $contents The contents of the file to be written.
 * @param $location Filesystem path where the file should be written.
 */
function asterisk_write_file($contents, $location) {
  $fh = fopen($location, 'w');
  $ret = fwrite($fh, $contents);
  fclose($fh);
  return $ret ? TRUE : FALSE;
}

/**
 * Converts an mp3 or regular wav file to an Asterisk compatible wav file
 * @param $orig_file
 *   Fullpath to the original file
 * @param $orig_filemime
 *   The mimetype of the original file
 * @param $converted_file
 *   (Desired) Fullpath to the converted file
 * @param $user
 *   The Drupal host site.
 * @return
 *   The return value of the conversion program (mpg123 or sox)
 */
function convert_sound_file($orig_file, $orig_filemime, $converted_file, $user) {
  $tempdir = DRUPAL_ACCOUNTS_DIR . DIRECTORY_SEPARATOR . $user . DIRECTORY_SEPARATOR .'cache';

  // Try to detect an mp3 file. If it is an mp3 file, then convert it to a wav
  // We don't make the wav directly in case we are actually using mpg321
  // (which doesn't have all these options)
  if (strpos($orig_filemime, 'mp') !== FALSE) {
    $temp_file = tempnam($tempdir, 'drupal_mp3_convert');
    system(MPG123 ." -q -w '$temp_file' '$orig_file'", $ret);
    $orig_file = $temp_file;
  }
  // We have a wav file, convert it to an Asterisk compatible wav file
  system(escapeshellcmd(SOX ." '$orig_file' -r 8000 -c 1 -w '$converted_file'"), $ret);

  // If there is a temp file, delete it
  if (isset($temp_file)) {
    unlink($temp_file);
  }
  return $ret;
}

/**
 * Fetches a file from the server originating the call if necessary.
 * @param $filename
 *   Name of the file as it appears on the asterisk server.
 * @param $file_url
 *   Full URL to access the file on the drupal site.
 * @param $user
 *   The Drupal host site.
 */
function fetch_file($filename, $file_url, $user) {

  $tempdir = DRUPAL_ACCOUNTS_DIR . DIRECTORY_SEPARATOR . $user . DIRECTORY_SEPARATOR . 'cache';
  $filename = $tempdir . DIRECTORY_SEPARATOR . $filename . '.wav';

  // if we haven't already downloaded the file
  if (!(is_file($filename) && is_readable($filename))) {

    $temp_filename = tempnam($tempdir, 'drupal_playback_');
    $temp_file   = fopen($temp_filename, 'w');

    $remote_file = fopen($file_url, 'r');

    if ($remote_file && $temp_file) {

      // save the remote file to a temporary file
      while (!feof($remote_file)) {
        $buffer = fgets($remote_file, 4096);
        fwrite($temp_file, $buffer);
      }
      fclose($remote_file);
      fclose($temp_file);

      // Asterisk uses 8kHz mono wav
      // Asterisk will .wav to the filename when searching
      convert_sound_file($temp_filename, $file['filemime'], $filename, $user);
      unlink($temp_filename);
    }
  }
}

/**
 * Converts a recorded file to XML-RPC base64, and adds it's information to
 * the existing file information.
 *
 * @param $user The Drupal host.
 * @param $file An array of file information.
 *
 * @return An array of file information ready to send via XML-RPC.
 */
function generate_recorded_file($user, $file) {

  $return = FALSE;

  $filepath = $file['asterisk_filepath'];
  if (file_exists($filepath)) {
    $file['bits'] = '';
    $file['filesize'] = filesize($filepath);
    if ($fh = fopen($filepath, 'r')) {
      while (!feof($fh)) {
        $file['bits'] .= fread($fh, 8192);
      }
      fclose($fh);

      $file['bits'] = xmlrpc_base64($file['bits']);
      $file['filemime'] = 'audio/x-wav';

      $return = $file;
    }
  }

  return $file;
}

/**
 * Writes an ini file based on an associative array of data.  The assoicative array
 * is two layers deep, it treats the first keys as section names. this file is in
 * a format that can be read using parse_ini_file.
 *
 * This function is derivative of code posted to the PHP Manual notes by Nick Deppe.
 *
 * @param $path The filepath, including filename, of the file to be written.
 * @param $content_array An associative array of data.
 * @return True if the file is written successfully.
 */
function write_ini_file($path, $content_array) {

 foreach($content_array as $key => $item) {
   if(is_array($item)) {
     $content .= "[$key]\n";
     foreach ($item as $key2 => $item2) {
       if(is_numeric($item2) || is_bool($item2))
         $content .= "$key2=$item2\n";
       else
         $content .= "$key2=\"$item2\"\n";
     }
     $content .= "\n";
   } else {
     if(is_numeric($item) || is_bool($item))
       $content .= "$key=$item\n";
     else
       $content .= "$key=\"$item\"\n";
   }
 }

 if(!$handle = fopen($path, 'w')) {
   return FALSE;
 }

 if(!fwrite($handle, $content)) {
   return FALSE;
 }

 fclose($handle);
 return TRUE;

}

/**
 * XML-RPC callbacks
 */

/**
 * Process a new call from a Drupal site.
 *
 * @param $user The username for the Drupal site.
 * @param $pass The password for the Drupal site.
 * @param $filetypes An associtive array representing the file types and files to validate
 * for existence on the Asterisk server.
 *
 * @return An associative array of all checked files that exists on the Asterisk server.
 */
function asterisk_check_files($user, $pass, $filetypes) {

  $return = asterisk_validate_host($user, $pass);

  if (isset($return['success'])) {
    $return = asterisk_validate_user_directories($user);
    if ($return === TRUE) {
      $return = array();
      foreach ($filetypes as $type => $files) {
        switch ($type) {
          case 'playback_file':
            $tempdir = DRUPAL_ACCOUNTS_DIR . DIRECTORY_SEPARATOR . $user . DIRECTORY_SEPARATOR .'cache';
            $return['playback_file'] = array();
            foreach ($files as $key => $file) {
            	if (file_exists($tempdir . DIRECTORY_SEPARATOR . $file)) {
                $return['playback_file'][$key] = TRUE;
              }
            }

            break;
        }
      }
    }
  }

  return $return;
}

/**
 * Process a new call from a Drupal site.
 *
 * @param $user The username for the Drupal site.
 * @param $pass The password for the Drupal site.
 * @param $files An associative array representing the call files and
 * auxilliary actions for Asterisk to process.
 *
 * @return An associative array of all successfully process actions.
 */
function asterisk_process_new_calls($user, $pass, $calls) {

  $return['callfile'] = array();
  $return['playback_file'] = array();

  $result = asterisk_validate_host($user, $pass);

  if (isset($result['success'])) {
    $result = asterisk_validate_user_directories($user);
    if ($result === TRUE) {
      foreach ($calls as $type => $files) {
        switch ($type) {
          case 'playback_file':
            foreach ($files as $key => $file) {
            	$result = asterisk_process_playback_message($user, $file);
            	if ($result === TRUE) {
                $return['playback_file'][$key] = TRUE;
              }
              else {
                $return['playback_file'][$key] = $result;
              }
            }
            break;
          case 'callfile';
            foreach ($files as $key => $file) {
            	$result = asterisk_write_call_file($user, $file);
            	if ($result === TRUE) {
                $return['callfile'][$key] = TRUE;
              }
              else {
                $return['callfile'][$key] = $result;
              }
            }
            break;
        }
      }
    }
    else {
      $return = $result;
    }
  }
  else {
    $return = $result;
  }

  return $return;
}

/**
 * Sends a waiting message to the appropriate Drupal site.
 *
 * @param $user The username for the Drupal site.
 * @param $pass The password for the Drupal site.
 * @param $message_key The password for the Drupal site.
 *
 * @return An array of information representing the message.
 */
function asterisk_send_message($user, $pass, $message_key) {

  $return = asterisk_validate_host($user, $pass);

  if (isset($return['success'])) {
    $return = asterisk_validate_user_directories($user);
    if ($return === TRUE) {
      $return = asterisk_retrieve_user_message($user, $message_key);
    }
  }

  return $return;
}

/**
 * Test the XML-RPC connection for a Drupal site.
 *
 * @param $user The username for the Drupal site.
 * @param $pass The password for the Drupal site.
 *
 * @return True if the user connection was successful.
 */
function asterisk_test_connection($user, $pass) {

  $return = asterisk_validate_host($user, $pass);
  if (isset($return['success'])) {
    $result = asterisk_validate_user_directories($user);
    if ($result !== TRUE) {
      $return = $result;
    }
  }

  return $return;
}

/**
 * XML-RPC methods for Drupal/Asterisk integration
 *
 * @return An array of XML-RPC methods.
 */
function _asterisk_methods() {
  return array(
    array(
      'asterisk.send.NewCalls',
      'asterisk_process_new_calls',
      array('struct', 'string', 'string', 'struct'),
      t('writes a constructed call file to the Asterisk outgoing spool')
    ),
    array(
      'asterisk.get.Message',
      'asterisk_send_message',
      array('struct', 'string', 'string', 'string'),
      t('retrieves a waiting message for the specified user.')
    ),
    array(
      'asterisk.test.Connection',
      'asterisk_test_connection',
      array('struct', 'string', 'string'),
      t('tests the connection to the Asterisk XML-RPC server.')
    ),
    array(
      'asterisk.check.Files',
      'asterisk_check_files',
      array('struct', 'string', 'string', 'struct'),
      t('checks for existence of passed files on the Asterisk server.')
    ),
  );
}

function asterisk_debug_to_file($contents, $op = 'a') {
  $fh = fopen('/tmp/asterisk.debug', $op);
  $ret = fwrite($fh, $contents);
  fclose($fh);
  return $ret;
}
