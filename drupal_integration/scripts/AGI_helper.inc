<?php

/**
 * This is a helper file which sets up some basic definitions and functions that
 * will assist in quickly building new AGI scripts.  Include this at the top of
 * your custom AGI script.  You should make sure your custom script is as
 * follows:
 *
 * #!/usr/bin/php -q
 * <?php
 * set_time_limit(60);
 * include(/path/to/AGI_helper.inc);
 *
 * ...your custom stuff here...
 *
 * ?>
 *
 * In this case, replace '/usr/bin/php' with the path to you system's php executable,
 * and '/path/to/' with the path to this file.
 *
 * The -q switch turns off HTML error messages.
 *
 * Note that there should be no space between the opening php path declaration
 * and the opening <?php tag, as any blank lines may confuse Asterisk.  For the same
 * reason, also make sure there's no white space after the closing php tag.
 *
 * If the script could take longer than 60 seconds to run, increase the value in
 * set_time_limit appropriately.
 *
 * Portions of this code were taken, with permission, from:
 * Asterisk: The Future of Telephony, by Jim Van Meggelen, Jared Smith, and Leif Madsen.
 * Copyright 2005 O'Reilly Media, Inc., 0-596-00962-3.
 */

// Set this to TRUE if you want all Asterisk responses to be printed to the Asterisk console
// with a PASS or FAIL indicator.
define('DEBUG', FALSE);

// An empty string holder.  Use this as a placeholder for any required command arguments
// that you wish to pass an empty value for.
define('STR', '""');

// A carriage return holder.  This is automatically appended to any complete set of data
// you send to Asterisk if you use the helper functions below.
define('CR', chr(10));

// Create file handles if needed.
if (!defined('STDIN')) {
  define('STDIN', fopen('php://stdin', 'r'));
}
if (!defined('STDOUT')) {
  define('STDOUT', fopen('php://stdout', 'w'));
}
if (!defined('STDERR')) {
  define('STDERR', fopen('php://stderr','w'));
}

// Turn off output buffering--we don't want Asterisk to be stuck waiting for any
// commands.
ob_implicit_flush(FALSE);

// Turn off error reporting, as it could interfere with the AGI interface.
error_reporting(0);

/**
 * This section contains the main five functions that you can use in your custom scripts.
 */

/**
 * Gets the initial variables passed from Asterisk.
 *
 * @return An associative array of variables passed in from Asterisk, key = variable name, value = value.
 */
function get_variables() {
  $agi = array();
  while (!feof(STDIN)) {
    $temp = trim(fgets(STDIN, 4096));
    if ($temp == "" || $temp == "\n") {
      break;
    }
    $s = explode(':', $temp, 2);
    $name = str_replace('agi_', '', $s[0]);
    $agi[$name] = trim($s[1]);
  }

  return $agi;
}

/**
 * Dumps variables to standard error.  Good for debugging.
 *
 * @param $agi An associative array of initial variables, key = variable name, value = value.  If not provided, the initial Asterisk variables are used.
 */
function dump_variables($agi = NULL) {
  $agi = isset($agi) ? $agi : get_variables();
  foreach ($agi as $key => $value) {
  	write_error("-- $key = $value");
  }
}

/**
 * Sends a command from the script to Asterisk via STDOUT.
 *
 * This function requires at least one initial argument, which is the command to send
 * to Asterisk. Additional arguments may be included if the command you're passing uses
 * other arguments--pass them in the order in which they are required in the command. Note
 * that the carriage return required to terminate a command to Asterisk is automatically
 * added by this function.
 *
 * Use the STR constant if you need to pass an empty command argument.
 */
function write_command() {
  $args = func_get_args();
  if (count($args)) {
    $command = implode(' ', $args);
    fwrite(STDOUT, $command . CR);
    fflush(STDOUT);
  }
  else {
    write_error('Error: write_command function requires at least one argument.');
  }
}

/**
 * Writes error messages to the Asterisk console via STDERR. Note that the carriage return
 * required to terminate a command to Asterisk is automatically added by this function.
 *
 * @param $errors Either a string which is the single error to print to the screen, or an array of strings to be printed as seperate error messages.
 */
function write_error($errors = NULL) {
  if (is_string($errors)) {
    fwrite(STDERR, $errors . CR);
    fflush(STDERR);
  }
  elseif (is_array($errors)) {
    foreach ($errors as $error) {
    	write_error($error);
    }
  }
  else {
    write_error('Error: write_error function requires an argument, which must be a string or an array of strings.');
  }
}

/**
 * Gets the response sent from Asterisk, and parses it to determine if the sent command was successful.
 *
 * @return 0 if the command failed, the result if the command succeeded, -1 if an unexpected result.
 */
function get_response() {
  $response = trim(fgets(STDIN, 4096));
  return checkresponse($response, DEBUG);
}

/**
 * These are internal functions to assist the helper functions above.
 */

/**
 * Parses the response from asterisk and determines if the command succeeded, failed, or returned
 * an unexpected response.
 *
 * @param $response A string representing the response to check.
 * @param $debug A boolean value indicating if the results should be printed to the Asterisk console.
 * @return 0 if the command failed, the result if the command succeeded, -1 if an unexpected result.
 */
function checkresponse($response, $debug) {
  trim($response);
  if (preg_match('/^200/', $response)) {
    if (!preg_match('/result=(-?\d+)/', $response, $matches)) {
      if ($debug) {
        write_error("FAIL ($response)");
      }
      return 0;
    }
    else {
      if ($debug) {
        write_error('PASS ('. $matches[1] . ')');
      }
      return $matches[1];
    }
  }
  else {
    if ($debug) {
      write_error("FAIL (unexpected result $response)");
    }
    return -1;
  }
}

?>
