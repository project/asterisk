<?php

/**
 * @file
 * Provides configuration for paths needed by the Asterisk/Drupal scripts.
 */

/**
 * The following path settings depend on your Asterisk installation--the current settings
 * are merely a best guess.  Check your Asterisk installation to set these properly!
 *
 * Note: Put no trailing slashes on any of the path names.
 */

/**
 * Directories
 */

// The main asterisk configuration directory.
define('ASTERISK_CONF_DIR', '/etc/asterisk');

// The Asterisk call spool directory.
define('CALL_SPOOL_DIR', '/var/spool/asterisk/outgoing');

/**
 * Executables
 */

// The Asterisk executable.
define('ASTERISK', '/usr/sbin/asterisk');

// The Sox executable.
define('SOX', '/usr/bin/sox');

// The mpg123 executable.
define('MPG123', '/usr/bin/mpg123');
