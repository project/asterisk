<?php

/**
 * This is a customized version of Drupal's xmlrpc.php which can be installed on
 * a remote Asterisk server if Asterisk/Drupal integration is desired.
 *
 * Place this file somewhere on your webserver that's publicly accessible.
 *
 * NOTE: It is HIGHLY recommended to set up a secure webserver via SSL for this
 * XML-RPC server. The asterisk module is designed to always initiate any XML-RPC
 * calls to the Asterisk server when there could be sensitive data being transmitted.
 * If you do not have a security layer in place, usernames and passwords could be
 * intercepted, possibly allowing outsiders to place calls through your Asterisk
 * server! If you don't know how to set up a secure webserver, there are many good
 * tutorials to be found on the internet--acquaint yourself...  :)
 */

// BEGIN CONFIGURATION

// Edit the paths below to the location of their respective directories on the Asterisk
// server.  NOTE: You'll also need to enter these same paths at the top of the AGI.inc
// file found in the 'drupal_integration/scripts' directory.
define('DRUPAL_INTEGRATION_DIR', '/full/path/to/drupal_integration');
define('DRUPAL_ACCOUNTS_DIR', '/full/path/to/drupal_accounts');

// END CONFIGURATION

require_once(DRUPAL_INTEGRATION_DIR . DIRECTORY_SEPARATOR .'asterisk_config.inc');
require_once(DRUPAL_INTEGRATION_DIR . DIRECTORY_SEPARATOR .'asterisk_common.inc');
require_once(DRUPAL_INTEGRATION_DIR . DIRECTORY_SEPARATOR .'support.inc');
require_once(DRUPAL_INTEGRATION_DIR . DIRECTORY_SEPARATOR .'xmlrpc.inc');
require_once(DRUPAL_INTEGRATION_DIR . DIRECTORY_SEPARATOR .'xmlrpcs.inc');

// The main call to the XML-RPC server.
xmlrpc_server(_asterisk_methods());
