
This file describes how to go about setting up new Drupal clients on an
Asterisk server which is configured to support Drupal's asterisk.module.
The setup is fairly straightforward, and mostly automated. Here are the
few steps you'll need to perform to add a new Drupal site as a client.

Make sure you have everything set up properly on the Asterisk server as
detailed in INSTALL.txt of the Drupal asterisk module. For each Drupal
client you wish to add, run the 'create_new_account.sh' script as the root
user. The script will prompt you for the following information:

1. The user/group on the Asterisk server that runs the webserver process.  For
   Apache, you can find this by looking in httpd.conf for the 'User' setting.

2. The user/group on the Asterisk server that runs the Asterisk process.  You
   can find this by looking in /etc/asterisk/asterisk.conf for the 'runuser'
   setting.

3. The Drupal site's Asterisk username. This is generated automatically
   by asterisk.module from the base URL of the Drupal site on which it
   resides. It can be found by looking under the following path on the
   Drupal site itself: Administer » Asterisk integration » Settings.

4. The password for the Drupal site's access.  This is what you'll give to
   the site owner to put in their 'Asterisk server password' setting located
   at Administer » Asterisk integration » Settings.

The script will use the information you enter to create the directories and
files needed for account access.

These include:

   [username]: The main account directory, which is the Drupal site's Asterisk
               username.

   [username]/cache: A directory to store temporary files related to the 
                     client, like call files waiting to be moved to the
                     outgoing spool, or audio files that have been previously
                     requested for playback.

   [username]/messages: All waiting messages for the client are stored here
                        until they are picked up.

   [username]/recordings: All recordings are stored here until they are
                          uploaded to the client.

   [username]/account.conf: Stores metadata related to the account, such as
                            the account password.

Since all the information for each client is stored inside of the client's
main folder, it should be very easy to troubleshoot problems, or to move the
client's information to a new username (if they change URLs).	

